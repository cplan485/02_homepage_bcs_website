Recreate homepage from Barcelona Code School’s website: https://barcelonacodeschool.com


Squares background for the header in SVG format is available for you here: https://www.barcelonacodeschool.com/files/pics/cur_files/squares.svg

You don’t need to create any functionality, just purely presentational copy of all the page elements.

For the menu no need for the drop downs, only items which are visible on the screen.